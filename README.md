# Cameron's Windows Comfy Scripts and Files

-- no longer maintained as I have moved to Linux full time. Windows was a secondary Operating system for me in the first place.
    (Which is why I had this script to download and install a default set of apps. Another set of scripts I had ran the installs for my paid software.)

## Packages installed after initial choco powershell install:

- **wget** (web downloader)
- **Firefox** (Web browser)
- **7zip** (Archive Tool)
- **VLC** (Video / Audio player)
- **Notepad++** (Text Editor)
- **Gimp** (Photo Editor)
- **Choco Gui** (Chocolately GUI Tool)
- **vim** (CLI Text Editor)
- **SumatraPDF** (PDF Viewer)
- **O&OShutUp10** (Windows Telemetry shutter upper)
- **Wox** (Keyboard Focused Application Launcher)
- ~~**Sharex** (Screenshot and File Uploader)~~ Working on conflicts with .NET
- **Rainmeter** (Pretty stuff)
- **Deluge** (Torrent Client)
- **Everything Search** (Disk Search Tool)
- **Discord** (Chat client)
- **Steam** (Game Downloader)
- **Open Shell** (Start Menu Replacement) 
- **FileZilla** (FTP Client)
- **Office365** (Office Suite)
- **Joplin** (MarkDown Editor)
- **Gitahead** (Git GUI)
- **PrivateInternetAccess** (VPN Application)
## Files inlaid when cloning this git repo
- Assorted Wallpapers
- Non sensitive configs
- List of Firefox Addons
- Secondary Scripts to run / stop Ham Radio software. 
## Steps
- Download this repo
- Run "choco-install.ps1"
- Run "choco-pkg-install.bat"
- Open command prompt in working folder
- Run "Wallpaper-Mover.bat"

Done

