# Firefox Addon List
A list of Firefox addons that I always install.

- [Bitwarden](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- [Pay By Privacy](https://addons.mozilla.org/en-US/firefox/addon/pay-by-privacy-com/)
- [PrivacyBadger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
- [DarkReader](https://addons.mozilla.org/en-US/firefox/addon/darkreader/)